public class Queue_Array {
    //array queue
    static int maxQueueArray = 5, front = 0, back = 0;

    ////space queue
    static String queueAtm [] = new String [5];

    //isfull
    static boolean isFull(){
        if (back == maxQueueArray){
            return true;
        }else {
            return false;
        }
    }

    //isempty
    static boolean isEmpty(){
        if (back == 0){
            return true;
        }else {
            return false;
        }
    }

    //enqueue -> penambahan dari belakang
    static void enqueue (String data){
        if (isFull()){
            System.out.println("Antrian Full");
        }else {
            if (isEmpty()){
                queueAtm [0] = data;
                front++;
                back++;
            }else{
                queueAtm[back] = data;
                back++;
            }
        }
    }

    //dequeue -> pengurangan dari depan
    static void dequeue(){
        if (isEmpty()){
            System.out.println("Antrian Kosong");
        }else {
            // for (int i = 0; i < back; i++){
            //     queueAtm[i] = queueAtm [i+1];
            // }
            // back--;
            queueAtm[0] = queueAtm[1];
            queueAtm[1] = queueAtm[2];
            queueAtm[2] = queueAtm[3];
            queueAtm[3] = queueAtm[4];
            queueAtm[4] = null;
        }
        back--;
    }

    //count queue
    static int countQeueu(){
        if (isEmpty()){
            return 0;
        }else if (isFull()){
            return maxQueueArray;
        }else{
            return back;
        }
    }

    //destroy qeueu -> clear all
    static void destroyQeueu(){
        if (isEmpty()){
            System.out.println("Antrian Kosong");
        }else {
            for (int i = 0; i < maxQueueArray; i++){
                if (back > 1){
                    queueAtm[back - 1] = null;
                    back--;
                }else if (back == 1){
                    queueAtm[back - 1] = null;
                    back = 0;
                    front = 0;
                }
            }
        }
    }

    //print queue
    static void printQeueu(){
        System.out.println("Data Antrian ATM");
        if (isEmpty()){
            System.out.println("Antrian Kosong");
        }else{
            for ( int i = 0; i < queueAtm.length; i++) {
                if (queueAtm [i] != null){
                    System.out.println(i+1+". "+queueAtm[i]);
                }else {
                    System.out.println(i+1+". (kosong)");
                }
            }
            System.out.println("Jumlah Antrian : "+countQeueu()+"\n");
        }
    }

    public static void main(String[] args) {
        enqueue("Budi");
        enqueue("Andi");
        enqueue("Cahyo");

        printQeueu();
        
        enqueue("Dimas");
        enqueue("Nani");

        printQeueu();

        dequeue();

        printQeueu();

        destroyQeueu();

        printQeueu();       
    }
}
