import java.util.Scanner;

class antrianBank {
    String namaYgAntri;
    int umurYgAntri;
    antrianBank next;
}

public class Queue_Linkedlist {

    static antrianBank head, tail, current, newNode,  del = new antrianBank();
    static int maxQeue = 5;
    static int pilih = 0;
    
    static Scanner input = new Scanner (System.in);

    //count queue
    static int countQueueLL(){
        if (head == null){
            return 0;
        }else {
            int number = 0;
            current = head;
            while (current != null){
                current = current.next;
                number++;
            }
            return number;
        }
    }

    //isfull
    static boolean isFullLL(){
        if (countQueueLL() == maxQeue){
            return true;
        }else {
            return false;
        }
    }

    //isempty
    static boolean isEmptyLL(){
        if (countQueueLL() == 0){
            return true;
        }else {
            return false;
        }
    }

    //enqueue -> penambahan
    static void enqueueLL(String namaIn, int umurIn){
        if (isFullLL()){
            System.out.println("Antrian Fuln\n");
        }else {
            if (isEmptyLL()){
                head = new antrianBank();
                head.namaYgAntri = namaIn;
                head.umurYgAntri = umurIn;
                head.next = null;
                tail = head;
            }else {
                newNode = new antrianBank();
                newNode.namaYgAntri = namaIn;
                newNode.umurYgAntri = umurIn;
                newNode.next = null;
                tail.next = newNode;
                tail = newNode;
            }
        }
    }

    static void dequeueuLL(){
        if (isEmptyLL()){
            System.out.println("Antrian Kosong\n");
        }else {
            del = head;
            head = head.next;
            del.next = null;
            del = null;
        }
    }

    //clear all queue
    static void clearqueue(){
        if(isEmptyLL()){
            System.out.println("Antrian Kosong\n");
        }else{
            current = head;
            while( current != null ){
                del = current;
                current = current.next;
                del.next = null;
                del = null;
            }
            head = null;
            tail = null;
        }
    }

    //print queue
    static void printLL(){
        if (isEmptyLL()){
            System.out.println("Antrian Kosong\n");
        }else{
            int hitung=0;
            current = head;
            while (current != null){
                System.out.print("\n("+current.namaYgAntri+", "+current.umurYgAntri+")");
                current = current.next;
                hitung++;
                if (current != null){
                    System.out.print(" <- ");
                }
            }
            System.out.println("\nMax queue : "+maxQeue+", terisi "+countQueueLL()+", sisa "+(maxQeue-hitung));
            System.out.println();
        }
    }

    static void menu(){
        do{
            System.out.println("\n===Daftar Antrian Bank===");
            System.out.println("1. Change max queue");
            System.out.println("2. Enqueue");
            System.out.println("3. Dequeue");
            System.out.println("4. Qeueu Full?");
            System.out.println("5. Queue Empty?");
            System.out.println("6. Count queue ");
            System.out.println("7. Clear queue");
            System.out.println("8. Print queue");
            System.out.println("9. Exit");
            System.out.print("Masukan pilihan : ");
            pilih = input.nextInt();

            switch (pilih) {
                case 1 :   
                    Scanner input1 = new Scanner (System.in);           
                    System.out.print("Masukan max queue : ");
                    maxQeue = input1.nextInt();
                    System.out.println("Max queue = "+maxQeue);
                    menu();
                case 2 :
                    Scanner input2 = new Scanner (System.in);
                    System.out.print("Masukan nama :");
                    String namaIn = input2.nextLine();
                    System.out.print("Masukan umur :");
                    int umurIn = input2.nextInt();
                    enqueueLL(namaIn, umurIn);
                    printLL();
                    menu();                   
                case 3 :
                    dequeueuLL();
                    printLL();
                    menu();                  
                case 4 :
                    System.out.println("Apakah queue full : "+isFullLL());
                    menu();
                case 5 :
                    System.out.println("Apakah queue kosong : "+isEmptyLL());
                    menu();
                case 6 :
                    System.out.println("Jumlah queue : "+countQueueLL());
                    menu();
                case 7 :
                    clearqueue();
                    printLL();
                    menu();
                case 8 :
                    printLL();
                    menu();              
            }
        }while(pilih != 9);
        System.out.println("Exit...");
        System.exit(0);

    }

    public static void main(String[] args) {
        menu();    
    }
}
