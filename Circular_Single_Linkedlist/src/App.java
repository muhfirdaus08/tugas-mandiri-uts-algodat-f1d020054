class hewan {
    //komponen
    String nama_hewan, jenis_hewan;
    hewan next;
}

public class App {

    static hewan head, tail, current, newNode, del = new hewan();

    public static boolean isEmpty(){
        return (head == null);
    }
    
    //make circular_sll
    static void make_circular_Sll(String namaHewan, String jenisHewan){
        head = new hewan();
        head.nama_hewan = namaHewan;
        head.jenis_hewan = jenisHewan;
        tail = head;
        tail.next = head;
    }

    //add first circular_sll
    static void addFirst_circular_Sll(String namaHewan, String jenisHewan){
        if (isEmpty()){
            System.out.println("Circular Single Linked List Kosong");
        }else {
            newNode = new hewan();
            newNode.nama_hewan = namaHewan;
            newNode.jenis_hewan = jenisHewan;
            newNode.next = head;
            tail.next = newNode;
            head = newNode;
        }     
    }

    //add middle circular_sll
    static void addMiddle_circular_Sll(String namaHewan, String jenisHewan, int posisi){
        if (isEmpty()){
            System.out.println("Circular Single Linked List Kosong");
        }else{
            newNode = new hewan();
            newNode.nama_hewan = namaHewan;
            newNode.jenis_hewan = jenisHewan;
            if (posisi == 1){
                System.out.println("Posisi ke-"+posisi+" tidak bisa ditambahkan");
            }else{
                current = head;
                int number = 1;
                while (number < posisi - 1){
                    current = current.next;
                    number++;
                }
                newNode.next = current.next;
                current.next = newNode;
            }
        }       
    }

    //add last circular_sll
    static void addLast_circular_Sll(String namaHewan, String jenisHewan){
        if (isEmpty()){
            System.out.println("Circular Single Linked List Kosong");
        }else{
            newNode = new hewan();
            newNode.nama_hewan = namaHewan;
            newNode.jenis_hewan = jenisHewan;
            newNode.next = head;
            tail.next = newNode;
            tail = newNode;
        }    
    }

    //delete first circular sll 
    static void delete_First (){
        if (isEmpty()){
            System.out.println("Circular Single Linked List Kosong");
        }else {
            del = head;
            head = head.next;
            tail.next = head;
            del = null;
        }
    }

    //delete middle curcular sll
    static void delete_Middle_circular_Sll(int posisi){
        if (isEmpty()){
            System.out.println("Circular Single Linked List Kosong");
        }else{
            if (posisi == 1){
                System.out.println("Posisi ke-"+posisi+" tidak bisa dihilangkan");
            }else{
                current = head;
                int number = 1;
                while (number < posisi - 1 ){
                    current = current.next;
                    number++;
                }
                del = current.next;
                current.next = del.next;
                del = null;
            }
        }       
    }

    //delete last circular sll
    static void delete_Last(){
        if (isEmpty()){
            System.out.println("Circular Single Linked List Kosong");
        }else{
            del = tail;
            current = head;
            while (current.next != tail){
                current = current.next;
            }
            tail = current;
            tail.next = head;
            del = null;
        }
    }

    //print circular_sll
    static void print_circular_Sll (){
        System.out.println("Circular Single Linked List");
        current = head;
        while (current.next != head){
            System.out.println("Nama hewan  : "+current.nama_hewan);
            System.out.println("Jenis hewan : "+current.jenis_hewan);
            System.out.println("------------------------------------");
            current = current.next;
        }
        System.out.println("Nama hewan  : "+current.nama_hewan);
        System.out.println("Jenis hewan : "+current.jenis_hewan+"\n");
    }

    public static void main(String[] args) {
        make_circular_Sll("Ayam", "Unggas");
        print_circular_Sll();

        addFirst_circular_Sll("Sapi", "Mamalia");
        print_circular_Sll();

        addLast_circular_Sll("Bunglon", "Reptil");
        print_circular_Sll();

        addMiddle_circular_Sll("Bebek", "Unggas", 2);
        print_circular_Sll();

        addMiddle_circular_Sll("Kutilang", "Unggas", 6);
        print_circular_Sll();

        delete_Middle_circular_Sll(4);
        print_circular_Sll();

        addMiddle_circular_Sll("Gajah", "Mamalia", 10);
        print_circular_Sll();
    }
}
