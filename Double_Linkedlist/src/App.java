class Mahasiswa {
    //komponen
    String nama, kelas;
    Mahasiswa next, prev;

    Mahasiswa (String nama, String kelas){
        this.nama = nama;
        this.kelas = kelas;
    }   
}

public class App {
    
    static Mahasiswa head, tail, current, newNode, del, after = new Mahasiswa("","");

    public static boolean isEmpty(){
        return (head == null);
    }

    //buat dll
    static void make_Dll(String nama, String kelas){
        head = new Mahasiswa(nama, kelas);
        head.prev = null;
        head.next = null;
        tail = head;
    }

    //count dll
    static int count_Dll (){
        current = head;
        int jumlah = 0;
        while (current != null){
            jumlah++;
            current = current.next;
        }
        return jumlah;
    }

    //add first dll
    static void add_FirstDLL (String nama, String kelas){
        if (isEmpty()){
            System.out.println("Double Linked List Kosong");
        }else {
            newNode = new Mahasiswa(nama, kelas);
            newNode.next = head;
            head.prev = newNode;
            head = newNode;
        }
    }

    //add middle dll
    static void add_MiddleDll (String nama, String kelas, int posisi){
        if (isEmpty()){
            System.out.println("Double Linked List Kosong");
        }else {
            newNode = new Mahasiswa(nama, kelas);
            if (posisi < 1 || posisi >= count_Dll()){
                System.out.println("Posisi diluar jangkauan\n");
            }else if (posisi == 1){
                System.out.println("Posisi != 1\n");
            }else{
                current = head;
                int number = 1;
                while (number < posisi - 1){
                    current = current.next;
                    number++;
                }
                after = current.next;
                newNode.prev = current;
                newNode.next = after;
                current.next = newNode;
                after.prev = newNode;
            }
        }
    }

    //add last dll
    static void add_LastDLL (String nama, String kelas){
        if (isEmpty()){
            System.out.println("Double Linked List Kosong");
        }else {
            newNode = new Mahasiswa(nama, kelas);
            newNode.next = null; 
            tail.next = newNode;          
            tail = newNode;
        }
    }

    //delete first
    static void delete_FirstDLL (){
        if (isEmpty()){
            System.out.println("Double Linked List Kosong");
        }else {
            del = head;
            head = head.next;
            head.prev = null;
            del = null;
        }
    }

    //delete middle dll
    static void delete_MiddleDll (int posisi){
        if (isEmpty()){
            System.out.println("Double Linked List Kosong");
        }else {
            if (posisi < 1 || posisi >= count_Dll()){
                System.out.println("Posisi diluar jangkauan\n");
            }else if (posisi == 1){
                System.out.println("Posisi != 1\n");
            }else{
                current = head;
                int number = 1;
                while (number < posisi - 1){
                    current = current.next;
                    number++;
                }
                del = current.next;
                after = del.next;
                current.next = after;
                after.prev = current;
                del = null;
            }
        }
    }

    //delete last
    static void delete_LastDLL (){
        if (isEmpty()){
            System.out.println("Double Linked List Kosong");
        }else {
            del = tail;
            tail = tail.prev;
            tail.next = null;
            del = null;
        }
    }

    //search data by position
    static void search_DllbyPosition(int posisi){
        if (posisi < 1 || posisi > count_Dll()){
            System.out.println("Data Posisi ke "+posisi+" tidak ada");
        }else{
            current = head;
            int number = 1;
            if (number == posisi){
                System.out.println("Data posisi ke-"+posisi+" adalah");
                System.out.print("|"+current.nama+"\t\t |"+current.kelas+"\t |\n");      
            }else{
                while (number < posisi){
                    current = current.next;
                    number++;
                }
                System.out.println("Data posisi ke-"+posisi+" adalah");
                System.out.print("|"+current.nama+"\t\t |"+current.kelas+"\t |\n\n");      
            }     
        }
    }

    //search data by name
    static void search_DllbyName(String namaHewan, String jenisHewan){
        current = head;
        int number = 1;
        while (namaHewan != current.nama && jenisHewan != current.kelas){
            current = current.next;
            number++;
        }
        System.out.println("\nData ");
        System.out.print("|"+current.nama+"\t\t |"+current.kelas+"\t |\n");
        System.out.println("ada di posisi ke-"+number+"\n");            
    }

    // print 
    static void print_Dll (){
        if (isEmpty()){
            System.out.println("\nDouble Linked List Kosong");
        }else {
            System.out.println("\nDouble Linked List Mahasiswa");
            System.out.print("---------------------------------");
            System.out.print("\n|Nama           |Kelas          |");
            System.out.print("\n---------------------------------");
            current = head;
            while (current != null){
            System.out.print("\n|"+current.nama+"\t\t|"+current.kelas+"\t\t|");
            current = current.next;
            }
            System.out.print("\n---------------------------------");
            System.out.println("\nJumlah data linked list = " +count_Dll()+"\n");
        }
    }

    //main
    public static void main(String[] args) {
        
        make_Dll("Anton", "B");
        print_Dll();

        add_FirstDLL("Jaya", "A");
        print_Dll();

        add_FirstDLL("Budi", "C");
        print_Dll();

        add_LastDLL("Ani", "C");
        print_Dll();

        add_LastDLL("Angga", "B");
        print_Dll();

        delete_MiddleDll(2);
        print_Dll();

        search_DllbyName("Anton", "B");
        search_DllbyPosition(4);
    }
}
