//deklarasi
class hewan {
    //komponen
    String nama_hewan, jenis_hewan;
    hewan next;
}

public class App {

    static hewan head, tail, current, newNode, del, before = new hewan();
    
    static boolean isEmpty(){
        return (head == null);
    }
                
    //buat sll
    static void make_Sll(String namaHewan, String jenisHewan){
        head = new hewan();
        head.nama_hewan = namaHewan;
        head.jenis_hewan = jenisHewan;
        head.next = null;
        tail = head;
    }

    //count sll
    static int count_Sll (){
        current = head;
        int jumlah = 0;
        while (current != null){
            jumlah++;
            current = current.next;
        }
        return jumlah;
    }

    //add first sll
    static void add_First (String namaHewan, String jenisHewan){
        if (isEmpty()){
            System.out.println("Linked list kosong");
        }else{
            newNode = new hewan();
            newNode.nama_hewan = namaHewan;
            newNode.jenis_hewan = jenisHewan;
            newNode.next = head;
            head = newNode;
        }      
    }

    //add middle sll
    static void add_Middle (String namaHewan, String jenisHewan, int posisi){
        if (isEmpty()){
            System.out.println("Linked list kosong");
        }else {
            newNode = new hewan();
            newNode.nama_hewan = namaHewan;
            newNode.jenis_hewan = jenisHewan;

            //menghitung first node sampai node sebelum posisi
            if (posisi < 1 || posisi > count_Sll()){
                System.out.println("Posisi diluar jangkauan\n");
            }else if (posisi == 1){
                System.out.println("Posisi tidak ditengah atau diakhir\n");
            }else {
                current = head;
                int number = 1;
                while (number < posisi - 1 ){ // angka posisi minimal 2
                current = current.next;
                number++;
                }
            newNode.next = current.next;
            current.next = newNode;
            }     
        }   
    }

    //add last sll
    static void add_Last (String namaHewan, String jenisHewan){
        if (isEmpty()){
            newNode = new hewan();
            newNode.nama_hewan = namaHewan;
            newNode.jenis_hewan = jenisHewan;
            newNode.next = null;
            tail.next = newNode;
            tail = newNode;
        }else {
            System.out.println("Linked list kosong");
        }   
    }

    //delete first sll 
    static void delete_First (){
        if (isEmpty()){
            System.out.println("Linked list kosong");
        }else{
            del = head;
            head = head.next;
            del = null;
        }
    }

    //delete middle sll
    static void delete_Middle (int posisi){
        if (posisi < 1 || posisi > count_Sll()){
            System.out.println("Posisi diluar jangkauan\n");
        }else if (posisi == 1){
            System.out.println("Posisi tidak ditengah atau diakhir\n");
        }else {
            if (!isEmpty()){
                int number = 1;
                current = head;
                while (number <= posisi){
                    if (number == posisi - 1){ // angka posisi minimal 2
                        before = current;
                    }
                    if (number == posisi){
                        del = current;
                    }
                    current = current.next;
                    number++;
                }
                before.next = current;
                del = null;
            }else {
                System.out.println("Linked list kosong");
            }        
        }      
    }

    //delete last sll
    static void delete_Last(){
        if (isEmpty()){
            System.out.println("Linked list kosong");
        }else {
            del = tail;
            current = head;
            while (current.next != tail){
                current = current.next;
            }
            tail = current;
            tail.next = null;
            del = null;
        }      
    }

    //change first sll
    static void change_First (String namaHewan, String jenisHewan){
        head.nama_hewan = namaHewan;
        head.jenis_hewan = jenisHewan;
    }

    //change middle sll
    static void change_Middle (String namaHewan, String jenisHewan, int posisi){
        if (posisi < 1 || posisi > count_Sll()){
            System.out.println("Posisi diluar jangkauan\n");
        }else if (posisi == 1){
            System.out.println("Posisi tidak ditengah atau diakhir\n");
        }else {
            current = head;
            int number = 1;
            while (number < posisi){
                current = current.next;
                number++;
            }
            current.nama_hewan = namaHewan;
            current.jenis_hewan = jenisHewan;
        }
    }

    //change last sll
    static void change_Last (String namaHewan, String jenisHewan){
        tail.nama_hewan = namaHewan;
        tail.jenis_hewan = jenisHewan;
    }

    //search data by position
    static void search_SllbyPosition(int posisi){
        if (posisi < 1 || posisi > count_Sll()){
            System.out.println("Data Posisi ke "+posisi+" tidak ada");
        }else{
            current = head;
            int number = 1;
            if (number == posisi){
                System.out.print("|"+current.nama_hewan+"\t\t |"+current.jenis_hewan+"\t |\n");      
            }else{
                while (number < posisi){
                    current = current.next;
                    number++;
                }
                System.out.println("Data posisi ke-"+posisi+" adalah");
                System.out.print("|"+current.nama_hewan+"\t\t |"+current.jenis_hewan+"\t |\n");      
            }
        }       
    }

    //search data by name
    static void search_SllbyName(String namaHewan, String jenisHewan){
        current = head;
        int number = 1;
        while (namaHewan != current.nama_hewan && jenisHewan != current.jenis_hewan){
            current = current.next;
            number++;
        }
        System.out.println("\nData ");
        System.out.print("|"+current.nama_hewan+"\t\t |"+current.jenis_hewan+"\t |\n");
        System.out.println("ada di posisi ke-"+number+"\n");      
    }

    //print sll
    static void print_Sll (){
        System.out.println("Single Linked List Hewan");
        System.out.print("----------------------------------");
        System.out.print("\n|Nama Hewan      |Jenis Hewan    |");
        System.out.print("\n---------------------------------");
        current = head;
        while (current != null){
            System.out.println();
            System.out.print("|"+current.nama_hewan+"\t\t |"+current.jenis_hewan+"\t |");

            current = current.next;
        }
        System.out.print("\n---------------------------------");
        System.out.println("\nJumlah data linked list = " +count_Sll()+"\n");
    }

    public static void main(String[] args) {
        make_Sll("Ayam", "Unggas");
        print_Sll();

        add_First("Kucing", "Mamamlia");
        print_Sll();

        change_First("Bebek", "Unggas");
        print_Sll();

        add_First("Sapi", "Mamamlia");
        print_Sll();

        add_Middle("Komodo", "Reptil", 4);
        print_Sll();

        change_Middle("Elang", "Unggas", 2);
        print_Sll();

        search_SllbyPosition(3);
        search_SllbyName("Elang", "Unggas");     
    }
}
