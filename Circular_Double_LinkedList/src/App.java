class dataBuku {
    //komponen
    String namaBuku; 
    int jumHal;
    dataBuku next, prev;
}

public class App {

    static dataBuku head, tail, current, newNode, del, after = new dataBuku();

    public static boolean isEmpty(){
        return (head == null);
    }
    
    //make circular_sll
    static void make_circular_Sll(String namaBukuIn, int jumBukuIn){
        head = new dataBuku();
        head.namaBuku = namaBukuIn;
        head.jumHal = jumBukuIn;
        head.prev = head;
        head.next = head;
        tail = head;
    }

    //add first circular_sll
    static void addFirst_circular_Sll(String namaBukuIn, int jumBukuIn){
        if (isEmpty()){
            System.out.println("Circular Double Linked List Kosong");
        }else {
            newNode = new dataBuku();
            newNode.namaBuku = namaBukuIn;
            newNode.jumHal = jumBukuIn;
            newNode.next = head;
            newNode.prev = tail;
            tail.next = newNode;
            head.prev = newNode;
            head = newNode;
        }     
    }

    //add middle circular_sll
    static void addMiddle_circular_Sll(String namaBukuIn, int jumBukuIn, int posisi){
        if (isEmpty()){
            System.out.println("Circular Double Linked List Kosong");
        }else{
            newNode = new dataBuku();
            newNode.namaBuku = namaBukuIn;
            newNode.jumHal = jumBukuIn;
            if (posisi == 1){
                System.out.println("Posisi ke-"+posisi+" tidak bisa ditambahkan");
            }else{
                current = head;
                int number = 1;
                while (number < posisi - 1){
                    current = current.next;
                    number++;
                }
                after = current.next;
                current.next = newNode;
                newNode.next = after;
                after.prev = newNode;
                newNode.prev = current;
            }
        }       
    }

    //add last circular_sll
    static void addLast_circular_Sll(String namaBukuIn, int jumBukuIn){
        if (isEmpty()){
            System.out.println("Circular Double Linked List Kosong");
        }else{
            newNode = new dataBuku();
            newNode.namaBuku = namaBukuIn;
            newNode.jumHal = jumBukuIn;
            newNode.prev = tail;
            newNode.next = head;
            head.prev = newNode;
            tail.next = newNode;
            tail = newNode;
        }    
    }

    //delete first circular sll 
    static void delete_First (){
        if (isEmpty()){
            System.out.println("Circular Double Linked List Kosong");
        }else {
            del = head;
            head = head.next;
            tail.next = head;
            head.prev = tail;
            del = null;
        }
    }

    //delete middle curcular sll
    static void delete_Middle_circular_Sll(int posisi){
        if (isEmpty()){
            System.out.println("Circular Double Linked List Kosong");
        }else{
            if (posisi == 1 || posisi < 1){
                System.out.println("Posisi ke-"+posisi+" tidak bisa dihilangkan");
            }else{
                current = head;
                int number = 1;
                while (number < posisi - 1 ){
                    current = current.next;
                    number++;
                }
                del = current.next;
                after = del.next;
                after.prev = current;
                current.next = after;
                del = null;
            }
        }       
    }

    //delete last circular sll
    static void delete_Last(){
        if (isEmpty()){
            System.out.println("Circular Double Linked List Kosong");
        }else{
            del = tail;
            tail = tail.prev;
            tail.next = head;
            del = null;
        }
    }

    //print circular_sll
    static void print_circular_Sll (){
        if (isEmpty()){
            System.out.println("Circular Double Linked List Kosong");
        }else{
            System.out.println("\nCircular Double Linked List");
            current = head;
            while (current.next != head){
                System.out.println("Nama Buku   : "+current.namaBuku);
                System.out.println("JumHal Buku : "+current.jumHal);
                System.out.println("------------------------------------");
                current = current.next;
            }
            //print last node
            System.out.println("Nama Buku   : "+current.namaBuku);
            System.out.println("JumHal Buku : "+current.jumHal);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        make_circular_Sll("Fisika", 120);
        addFirst_circular_Sll("Biologi", 190);
        addLast_circular_Sll("Matematika", 125);
        addMiddle_circular_Sll("Agama", 200, 9);
        print_circular_Sll();

        delete_Middle_circular_Sll(10);
        print_circular_Sll();
    }
}
