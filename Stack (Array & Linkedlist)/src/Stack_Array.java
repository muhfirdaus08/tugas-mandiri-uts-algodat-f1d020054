public class Stack_Array {
    static int max = 5;
    static String buku[] = new String [5];
    static int top = 0;

    static boolean isFull(){
        if (top == max){
            return true;
        }else{
            return false;
        }
    }

    static boolean isEmpty(){
        if (top == 0){
            return true;
        }else{
            return false;
        }
    }

    static void pushArray (String data){
        if (isFull()){
            System.out.println("Data full");
        }else {
            buku[top] = data;
            top++;
        }
    }

    static void popArray(){
        if (isEmpty()){
            System.out.println("Data Kosong");
        }else{
            buku[top - 1 ] = null;
        }
    }

    static void printArray(){
        for (int i = max - 1; i >= 0; i--) {
            if(buku[i] != null){
                System.out.println("Data Stack Array");
                System.out.println("Data : "+buku[i]);
            }
        }
        System.out.println();     
    }
    
    static void peekArray (int posisi){
        if (isEmpty()){
            System.out.println("Data Kosong");
        }else{
            int index = top;
            for (int i = 0; i <= posisi; i++) {
                index--;
            }
            System.out.println("Data ke "+posisi+" : "+buku[index]);
        }
    }

    static void changeArray (String data, int posisi){
        if (isEmpty()){
            System.out.println("Data Kosong");
        }else{
            int index = top;
            for (int i = 0; i <= posisi; i++) {
                index--;
            }
            System.out.println(buku[index] = data);
        }
    }

    static int count(){
        if (isEmpty()){
            return 0;
        }else{
            return top;
        }
    }

    static void destroyArray(){
        for (int i = 0; i < top; i++) {
            buku[i] = null;
        }
        top = 0;
    }
    public static void main(String[] args) throws Exception {
        pushArray("Fisika");     
        pushArray("Matematika");
        pushArray("Biologi");
        pushArray("Kimia");
        pushArray("English");
        printArray();
        popArray();
        changeArray("Seni Budaya", 3);
        printArray();
        
        System.out.println("Apakah data full : "+isFull());
        System.out.println("Apakah data kosong : "+isEmpty());

        peekArray(3);
        System.out.println("Jumlah data : "+count());

        System.out.println();

        destroyArray();
        printArray();

        System.out.println("Apakah data full : "+isFull());
        System.out.println("Apakah data kosong : "+isEmpty());

        System.out.println();

    }
}
