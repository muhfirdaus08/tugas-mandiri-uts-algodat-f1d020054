class dataBuku{
    String namaBuku;
    int hargaBuku;

    dataBuku prev, next;
}

public class Stack_Linkedlist {
    
    static dataBuku head, tail, current, newNode, del = new dataBuku();
    static int maxBarang = 5;
    
    //make stack with dll
    static void makeBuku(String buku, int harga){
        head = new dataBuku();
        head.namaBuku = buku;
        head.hargaBuku = harga;
        head.next = null;
        head.prev = null;
        tail = head;
    }

    //count stack
    static int countBuku (){
        if (head == null){
            return 0;
        }else{
            int banyak = 0;
            current = head;
            while (current != null){
                current = current.next;
                banyak++;
            }
            return banyak;
        }
    }

    //stack full
    static boolean bukuFull(){
        if (countBuku() == maxBarang){
            return true;
        }else{
            return false;
        }
    }

    //stack empty
    static boolean bukuEmpty(){
        if (countBuku() == 0){
            return true;
        }else{
            return false;
        }
    }

    //add stack
    static void pushBuku (String buku, int harga){
        if (bukuFull()){
            System.out.println("Stack Full");
        }else {
            if (countBuku() == 0){
                makeBuku(buku, harga);
            }else{
                newNode = new dataBuku();
                newNode.namaBuku = buku;
                newNode.hargaBuku = harga;
                newNode.next = null;    
                newNode.prev = tail;
                tail.next = newNode;
                tail = newNode;
            }
        }
    }

    //remove stack
    static void removeBuku(){
        if (bukuEmpty()){
            System.out.println("Stack Kosong");
        }else {
            del = tail;
            tail = tail.prev;
            tail.next = null;
        }
    }

    //search stack
    static void searchBuku(int posisi){
        if (bukuEmpty()){
            System.out.println("Stack Kosong");
        }else {
            int number = 1;
            current = tail;
            while (number < posisi){
                current = current.prev;
                number++;
            }
            System.out.print("Data ke "+posisi+" adalah "+current.namaBuku+" -> "+current.hargaBuku+"\n\n");
        }
    }

    //change stack
    static void changeBuku(String buku, int harga, int posisi){
        if (bukuEmpty()){
            System.out.println("Stack Kosong");
        }else {
            int number = 1;
            current = tail;
            while (number < posisi){
                current = current.prev;
                number++;
            }
            current.namaBuku = buku;
            current.hargaBuku = harga;
        }
    }

    // destroy stack
    static void destroyBuku(){
        // current = head;
        // while (current != null){
        //     del = current;
        //     head = head.next;
        //     del = null;
        //     current = current.next;
        // }

        // algoritma lebih simple
        while (head != null){
            del = head;
            head = head.next;
            del = null;
        }
    }

    //print stack
    static void printBuku (){
        if (bukuEmpty()){
            System.out.println("Stack Kosong\n");
        }else{
            System.out.println("\nData Stack (Buku -> Harga)");
            System.out.println("--------------------------------------");
            current = tail;
            while (current != null){
                System.out.print(current.namaBuku+" -> "+current.hargaBuku);
                System.out.println("\n--------------------------------------");
                current = current.prev;
            }
            System.out.println();          
        }
    }

    //main
    public static void main(String[] args) {
        pushBuku("Fisika", 99000);
        printBuku();
        pushBuku("Matematika", 128000);
        pushBuku("Sejarah", 80000);
        changeBuku("Kimia", 75000, 3);
        printBuku();
        searchBuku(2);
        destroyBuku();
        System.out.println("Apakah data full : "+bukuFull());
        System.out.println("Apakah data kosong : "+bukuEmpty()+"\n");
        printBuku();
    }

}
